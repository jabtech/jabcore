declare class Config {
    static interpolate(str: string, params: any): any;
    static getInstance(): Config;
    private static _instance;
    environment: string;
    private constructor();
    loadConfig(): void;
    get(key: string, interpolateObject?: object): any;
    getAll(): any;
    getService(serviceName: string, serviceValues?: {}): any;
    save(data: object): void;
    refresh(): void;
    getEnvConfig(): any;
}
declare const config: Config;
export { config };
