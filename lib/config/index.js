"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
const lodash_1 = require("lodash");
const nconf = require("nconf");
const dotenv = require("dotenv");
class Config {
    static interpolate(str, params) {
        const names = lodash_1.keys(params);
        const vals = lodash_1.values(params);
        return new Function(...names, `return \`${str}\`;`)(...vals);
    }
    static getInstance() {
        if (!Config._instance) {
            Config._instance = new Config();
            Config._instance.loadConfig();
        }
        return Config._instance;
    }
    constructor() {
        dotenv.config();
        nconf.argv().env({
            separator: '_',
        });
        this.environment = lodash_1.toLower(nconf.get('NODE:ENV') || 'development');
    }
    loadConfig() {
        if (lodash_1.isEmpty(this.environment)) {
            throw new Error(`config environment is missing data. node:${this.environment}`);
        }
        nconf.file(this.environment, path.join(process.cwd(), `./config/${this.environment}.json`));
        nconf.file('default', path.join(process.cwd(), './config/default.json'));
    }
    get(key, interpolateObject = {}) {
        const configValue = nconf.get(key);
        if (!lodash_1.isEmpty(interpolateObject)) {
            interpolateObject = lodash_1.merge(interpolateObject, this.environment);
            if (typeof configValue === 'string') {
                return Config.interpolate(configValue, interpolateObject);
            }
        }
        return configValue;
    }
    getAll() {
        return nconf.get();
    }
    getService(serviceName, serviceValues = {}) {
        const serviceKey = `Endpoints:${serviceName}`;
        const serviceUrl = `${serviceKey}:Url`;
        if (nconf.get(serviceUrl)) {
            return this.get(serviceUrl, serviceValues);
        }
        return this.get(serviceKey, serviceValues);
    }
    save(data) {
        if (lodash_1.isEmpty(data)) {
            throw new Error('Config must not be empty. It will erase the config file');
        }
        const store = nconf.stores[this.environment];
        fs.writeFileSync(store.file, JSON.stringify(data, undefined, 2));
        this.refresh();
    }
    refresh() {
        dotenv.config();
        nconf.argv().env({
            separator: '_',
        });
        this.loadConfig();
    }
    getEnvConfig() {
        const store = nconf.stores[this.environment];
        const content = fs.readFileSync(store.file);
        try {
            return JSON.parse(content.toString('ascii'));
        }
        catch (error) {
            return error;
        }
    }
}
const config = Config.getInstance();
exports.config = config;
//# sourceMappingURL=index.js.map