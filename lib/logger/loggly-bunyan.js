"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BunyanLoggly = require("bunyan-loggly");
const config_1 = require("../config");
const lodash_1 = require("lodash");
class LogglyBunyan extends BunyanLoggly {
    static onSend(error, result, content) {
        if (error) {
            console.error('Error sending loggly log', error);
            return;
        }
    }
    constructor(logglyConfig, bufferLength, bufferTimeout, callback) {
        super(logglyConfig, bufferLength, bufferTimeout, callback || LogglyBunyan.onSend);
    }
    write(data) {
        const options = config_1.config.get(`Logging:${data.name}:Loggly`);
        if (!lodash_1.isEmpty(options)) {
            if (options.Enabled !== true) {
                return;
            }
        }
        super.write(data);
    }
}
exports.LogglyBunyan = LogglyBunyan;
module.exports = LogglyBunyan;
//# sourceMappingURL=loggly-bunyan.js.map