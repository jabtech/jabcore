import * as BunyanLoggly from 'bunyan-loggly';
export interface IBunyanLogglyConfig {
    subdomain: string;
    tags: string[];
    token: string;
}
export declare class LogglyBunyan extends BunyanLoggly {
    static onSend(error: any, result: any, content: any): void;
    constructor(logglyConfig: IBunyanLogglyConfig, bufferLength: number, bufferTimeout: number, callback: () => void);
    write(data: any): void;
}
