import * as BunyanLogger from 'bunyan';
export declare enum levels {
    trace = 10,
    debug = 20,
    info = 30,
    warn = 40,
    error = 50,
    fatal = 60
}
declare class Logger {
    static getInstance(): Logger;
    private static _instance;
    private _loggers;
    private _loggerConfig;
    private constructor();
    updateLevels(): void;
    getLogger(name?: string): BunyanLogger;
    private _createLogger;
    private _createDefaultLogger;
}
declare const logger: Logger;
declare const log: BunyanLogger;
export { log, logger };
