"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BunyanLogger = require("bunyan");
const BunyanLoggly = require("bunyan-loggly");
const config_1 = require("../config");
const lodash_1 = require("lodash");
var levels;
(function (levels) {
    levels[levels["trace"] = 10] = "trace";
    levels[levels["debug"] = 20] = "debug";
    levels[levels["info"] = 30] = "info";
    levels[levels["warn"] = 40] = "warn";
    levels[levels["error"] = 50] = "error";
    levels[levels["fatal"] = 60] = "fatal";
})(levels = exports.levels || (exports.levels = {}));
const LOGGER_CONFIG_KEY = 'Logging';
const DEFAULT_LOGGER = 'Default';
class Logger {
    static getInstance() {
        if (!Logger._instance) {
            Logger._instance = new Logger();
        }
        return Logger._instance;
    }
    constructor() {
        this._loggerConfig = config_1.config.get(LOGGER_CONFIG_KEY);
        this._loggers = {};
        lodash_1.forOwn(this._loggerConfig, (val, key) => {
            if (lodash_1.isEmpty(val)) {
                return;
            }
            this._loggers[key] = this._createLogger(key);
        });
    }
    updateLevels() {
        lodash_1.forOwn(this._loggerConfig, (loggerVal, loggerKey) => {
            lodash_1.forOwn(loggerVal, (streamVal, streamKey) => {
                if (lodash_1.has(levels, this._loggerConfig.Loggly)) {
                    const currentLogger = this.getLogger(loggerKey);
                    currentLogger.levels(streamKey, streamVal.Level);
                }
            });
        });
    }
    getLogger(name = DEFAULT_LOGGER) {
        if (lodash_1.isEmpty(this._loggers[name])) {
            if (name === DEFAULT_LOGGER) {
                this._loggers[DEFAULT_LOGGER] = this._createDefaultLogger();
            }
            else {
                throw new Error(`${name} logger not created. Created ${Object.keys(this._loggerConfig).join(', ')}`);
            }
        }
        return this._loggers[name];
    }
    _createLogger(configKey) {
        const options = config_1.config.get(`${LOGGER_CONFIG_KEY}:${configKey}`);
        const streams = [];
        if (lodash_1.isEmpty(options)) {
            throw new Error(`No config for ${configKey}`);
        }
        const PrettyStream = options.PrettyStream;
        if (!lodash_1.isEmpty(PrettyStream) && PrettyStream.Enabled === true && lodash_1.has(levels, PrettyStream.Level)) {
            const bunyanPrettyStream = require('bunyan-prettystream');
            const prettyStdOut = new bunyanPrettyStream();
            prettyStdOut.pipe(process.stdout);
            streams.push({
                level: PrettyStream.Level,
                name: 'PrettyStream',
                stream: prettyStdOut,
            });
        }
        const LogToStdout = options.LogToStdout;
        if (!lodash_1.isEmpty(LogToStdout) && LogToStdout.Enabled === true && lodash_1.has(levels, LogToStdout.Level)) {
            streams.push({
                level: LogToStdout.Level,
                name: 'LogToStdout',
                stream: process.stdout,
            });
        }
        const Loggly = options.Loggly;
        if (!lodash_1.isEmpty(Loggly) && Loggly.Enabled === true && lodash_1.has(levels, Loggly.Level)) {
            const token = config_1.config.get('Loggly:Token');
            if (lodash_1.isEmpty(token)) {
                throw new Error('Logger is missing loggly token');
            }
            const subdomain = config_1.config.get('Loggly:SubDomain');
            if (lodash_1.isEmpty(subdomain)) {
                throw new Error('Logger is missing loggly subdomain');
            }
            streams.push({
                level: Loggly.Level,
                name: 'Loggly',
                stream: new BunyanLoggly({
                    subdomain,
                    tags: [configKey],
                    token,
                }),
                type: 'raw',
            });
        }
        return BunyanLogger.createLogger({
            name: configKey,
            serializers: {
                err: BunyanLogger.stdSerializers.err,
                error: BunyanLogger.stdSerializers.err,
                req: BunyanLogger.stdSerializers.req,
                res: BunyanLogger.stdSerializers.res,
            },
            streams,
        });
    }
    _createDefaultLogger() {
        const streams = [];
        streams.push({
            level: BunyanLogger.ERROR,
            name: 'LogToStdout',
            stream: process.stdout,
        });
        return BunyanLogger.createLogger({
            name: DEFAULT_LOGGER,
            serializers: {
                err: BunyanLogger.stdSerializers.err,
                error: BunyanLogger.stdSerializers.err,
                req: BunyanLogger.stdSerializers.req,
                res: BunyanLogger.stdSerializers.res,
            },
            streams,
        });
    }
}
const logger = Logger.getInstance();
exports.logger = logger;
const log = logger.getLogger();
exports.log = log;
//# sourceMappingURL=index.js.map