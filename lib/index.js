"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
exports.config = config_1.config;
const logger_1 = require("./logger");
exports.log = logger_1.log;
exports.logger = logger_1.logger;
//# sourceMappingURL=index.js.map