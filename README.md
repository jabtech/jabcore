# jabcore

Core libs for typescript nodejs apps

## logger

Uses bunyan logger with some streams such as pretty stream and loggly

### For Loggly, please place creds in config
  "Loggly": {
    "Token": "", 
    "SubDomain": ""
  },
  
  You can use process.env.Loggly_Token and Loggly_SubDomain as well to keep your creds out of config

## config

Uses nconf for configuration management. Will pick up all json configs from /config dir of project

### Name must match node_env exp: production.json

There should be a default.json as a default