import * as fs from 'fs';
import * as path from 'path';
import { isEmpty, keys, merge, toLower, values } from 'lodash';
import * as nconf from 'nconf';
import * as dotenv from 'dotenv';

class Config {
  public static interpolate(str: string, params: any) {
    const names = keys(params);
    const vals = values(params);
    return new Function(...names, `return \`${str}\`;`)(...vals);
  }
  public static getInstance(): Config {
    if (!Config._instance) {
      Config._instance = new Config();
      Config._instance.loadConfig();
    }
    return Config._instance;
  }
  private static _instance: Config;

  public environment: string;

  private constructor() {
    dotenv.config();
    nconf.argv().env({
      separator: '_',
    });
    this.environment = toLower(nconf.get('NODE:ENV') || 'development');
  }

  /**
   * @description Loads config and overrides it using the overrides arg
   * @param {object} overrides - config values to override
   */
  public loadConfig() {
    if (isEmpty(this.environment)) {
      throw new Error(`config environment is missing data. node:${this.environment}`);
    }

    nconf.file(this.environment, path.join(process.cwd(), `./config/${this.environment}.json`));

    nconf.file('default', path.join(process.cwd(), './config/default.json'));
  }

  /**
   *
   * @description Gets the given config value given the key you provide. Will also replace values using
   * the interpolateObject arg. Will automaticlly include env, domain and seperator.
   * @param {string} key - The key of the values you need from the config. Use : for nested objects
   * @param {object} interpolateObject - Replacement values you want to use
   */
  public get(key: string, interpolateObject: object = {}) {
    const configValue = nconf.get(key);

    // Don't interpolate if not a endpoints config value
    if (!isEmpty(interpolateObject)) {
      interpolateObject = merge(interpolateObject, this.environment);
      if (typeof configValue === 'string') {
        return Config.interpolate(configValue, interpolateObject);
      }
    }

    return configValue;
  }

  public getAll() {
    return nconf.get();
  }

  /**
   *
   * @description Gets the url for the service and replaces any values using keys arg
   * @param {string} serviceName - Key in services config
   * @param {object} serviceValues - Values to replace (Optional)
   */
  public getService(serviceName: string, serviceValues = {}) {
    const serviceKey = `Endpoints:${serviceName}`;
    const serviceUrl = `${serviceKey}:Url`;

    if (nconf.get(serviceUrl)) {
      return this.get(serviceUrl, serviceValues);
    }

    return this.get(serviceKey, serviceValues);
  }

  public save(data: object) {
    if (isEmpty(data)) {
      throw new Error('Config must not be empty. It will erase the config file');
    }
    const store = nconf.stores[this.environment];
    fs.writeFileSync(store.file, JSON.stringify(data, undefined, 2));
    this.refresh();
  }

  public refresh() {
    dotenv.config();
    nconf.argv().env({
      separator: '_',
    });
    this.loadConfig();
  }

  public getEnvConfig() {
    const store = nconf.stores[this.environment];
    const content = fs.readFileSync(store.file);
    try {
      return JSON.parse(content.toString('ascii'));
    } catch (error) {
      return error;
    }
  }
}

const config = Config.getInstance();

export { config };
