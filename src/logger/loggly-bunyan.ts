import * as BunyanLoggly from 'bunyan-loggly';
import { config } from '../config';
import { isEmpty } from 'lodash';

export interface IBunyanLogglyConfig {
  subdomain: string;
  tags: string[];
  token: string;
}

export class LogglyBunyan extends BunyanLoggly {
  public static onSend(error: any, result: any, content: any): void {
    if (error) {
      // tslint:disable-next-line:no-console
      console.error('Error sending loggly log', error);
      return;
    }
  }

  constructor(logglyConfig: IBunyanLogglyConfig, bufferLength: number, bufferTimeout: number, callback: () => void) {
    super(logglyConfig, bufferLength, bufferTimeout, callback || LogglyBunyan.onSend);
  }

  public write(data: any): void {
    const options = config.get(`Logging:${data.name}:Loggly`);
    if (!isEmpty(options)) {
      if (options.Enabled !== true) {
        return;
      }
    }

    super.write(data);
  }
}

module.exports = LogglyBunyan;
