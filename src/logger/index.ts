import * as BunyanLogger from 'bunyan';
import * as BunyanLoggly from 'bunyan-loggly';
import { config } from '../config';
import { forOwn, has, isEmpty } from 'lodash';

export enum levels {
  trace = 10,
  debug = 20,
  info = 30,
  warn = 40,
  error = 50,
  fatal = 60,
}

interface ILoggers {
  [key: string]: BunyanLogger;
}

const LOGGER_CONFIG_KEY = 'Logging';
const DEFAULT_LOGGER = 'Default';

class Logger {
  public static getInstance(): Logger {
    if (!Logger._instance) {
      Logger._instance = new Logger();
    }
    return Logger._instance;
  }
  private static _instance: Logger;
  private _loggers: ILoggers;
  private _loggerConfig: any;
  private constructor() {
    this._loggerConfig = config.get(LOGGER_CONFIG_KEY);
    this._loggers = {};
    forOwn(this._loggerConfig, (val, key) => {
      if (isEmpty(val)) {
        return;
      }
      this._loggers[key] = this._createLogger(key);
    });
  }

  public updateLevels(): void {
    forOwn(this._loggerConfig, (loggerVal, loggerKey) => {
      forOwn(loggerVal, (streamVal, streamKey) => {
        if (has(levels, this._loggerConfig.Loggly)) {
          const currentLogger = this.getLogger(loggerKey);
          currentLogger.levels(streamKey, streamVal.Level);
        }
      });
    });
  }

  public getLogger(name: string = DEFAULT_LOGGER): BunyanLogger {
    if (isEmpty(this._loggers[name])) {
      if (name === DEFAULT_LOGGER) {
        this._loggers[DEFAULT_LOGGER] = this._createDefaultLogger();
      } else {
        throw new Error(`${name} logger not created. Created ${Object.keys(this._loggerConfig).join(', ')}`);
      }
    }
    return this._loggers[name];
  }

  private _createLogger(configKey: string): BunyanLogger {
    const options = config.get(`${LOGGER_CONFIG_KEY}:${configKey}`);

    const streams = [];
    if (isEmpty(options)) {
      throw new Error(`No config for ${configKey}`);
    }

    const PrettyStream = options.PrettyStream;
    if (!isEmpty(PrettyStream) && PrettyStream.Enabled === true && has(levels, PrettyStream.Level)) {
      const bunyanPrettyStream = require('bunyan-prettystream');
      const prettyStdOut = new bunyanPrettyStream();
      prettyStdOut.pipe(process.stdout);
      streams.push({
        level: PrettyStream.Level,
        name: 'PrettyStream',
        stream: prettyStdOut,
      });
    }

    const LogToStdout = options.LogToStdout;
    if (!isEmpty(LogToStdout) && LogToStdout.Enabled === true && has(levels, LogToStdout.Level)) {
      streams.push({
        level: LogToStdout.Level,
        name: 'LogToStdout',
        stream: process.stdout,
      });
    }

    const Loggly = options.Loggly;
    if (!isEmpty(Loggly) && Loggly.Enabled === true && has(levels, Loggly.Level)) {
      const token = config.get('Loggly:Token');
      if (isEmpty(token)) {
        throw new Error('Logger is missing loggly token');
      }
      const subdomain = config.get('Loggly:SubDomain');
      if (isEmpty(subdomain)) {
        throw new Error('Logger is missing loggly subdomain');
      }

      streams.push({
        level: Loggly.Level,
        name: 'Loggly',
        stream: new BunyanLoggly({
          subdomain,
          tags: [configKey],
          token,
        }),
        type: 'raw',
      });
    }

    return BunyanLogger.createLogger({
      name: configKey,
      serializers: {
        err: BunyanLogger.stdSerializers.err,
        error: BunyanLogger.stdSerializers.err,
        req: BunyanLogger.stdSerializers.req,
        res: BunyanLogger.stdSerializers.res,
      },
      streams,
    });
  }

  private _createDefaultLogger(): BunyanLogger {
    const streams: BunyanLogger.Stream[] = [];
    streams.push({
      level:  BunyanLogger.ERROR,
      name: 'LogToStdout',
      stream: process.stdout,
    });

    return BunyanLogger.createLogger({
      name: DEFAULT_LOGGER,
      serializers: {
        err: BunyanLogger.stdSerializers.err,
        error: BunyanLogger.stdSerializers.err,
        req: BunyanLogger.stdSerializers.req,
        res: BunyanLogger.stdSerializers.res,
      },
      streams,
    });

  }
}

const logger = Logger.getInstance();
const log = logger.getLogger();
export { log, logger};
